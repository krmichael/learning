const filter = (array, fn) =>
  array.reduce((acc, cur) => {
    if (!fn(cur)) return acc;

    return [...acc, fn(cur)];
  }, []);

const filtered = filter([1, 5, 10, 7], elem => elem >= 7);

console.log(filtered);
