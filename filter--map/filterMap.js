const filterMap = (array, fnFilter, fnMap) =>
  array.reduce((acc, cur) => {
    if (!fnFilter(cur)) return acc;
    return [...acc, fnMap(cur)];
  }, []);

const filteredMap = filterMap(
  [10, 20, 30],
  elem => elem >= 20,
  elem => elem * 2
);

console.log(filteredMap);
