const map = (array, fn) =>
  array.reduce((acc, cur) => {
    return [...acc, fn(cur)];
  }, []);

const multi = map([1, 3, 5], elem => elem * 2);

console.log(multi);
