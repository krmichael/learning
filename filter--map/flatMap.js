const flatMap = (array, fn) =>
  array.reduce((acum, elem) => {
    if (Array.isArray(elem)) return [...acum, ...elem.map(fn)];
    return [...acum, fn(elem)];
  }, []);

console.log(flatMap([[1, 2], 3, [4, 5]], elem => elem * 2));
