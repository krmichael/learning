const unique = array =>
  array.reduce((acum, elem) => {
    if (acum.includes(elem)) return acum;

    return [...acum, elem];
  }, []);

console.log(unique([1, 2, 1, 3, 5, 9, 3]));
//https://itnext.io/powerful-js-reduce-function-58cf47edcb8
