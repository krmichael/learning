const reverse = array =>
  array.reduce((acum, elem) => {
    return [elem, ...acum];
  }, []);

console.log(reverse([9, 5, 2, 1]));
