const sort = array =>
  array.reduce((acum, elem) => {
    const lesserOrEqual = acum.filter(item => item <= elem);
    const greater = acum.filter(item => item > elem);

    return [...lesserOrEqual, elem, ...greater];
  }, []);

console.log(sort([3, 1, 5, 9, 4, 2, 4, 1]));
