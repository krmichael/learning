const compress = array =>
  array.reduce((acc, cur) => {
    return acc.concat(cur);
  }, []);

const uneArrays = compress([[1, 3, 5], [10, 4, 7], [0, 8, 12]]);

//[]
//[].concat([1, 3, 5])
//[1, 3, 5].concat([10, 4, 7])
//[1, 3, 5, 10, 4, 7].concat([0, 8, 12])

console.log(uneArrays); //[ 1, 3, 5, 10, 4, 7, 0, 8, 12 ]
