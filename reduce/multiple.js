const sumProduct = array =>
  array.reduce((acc, cur) => {
    const [number, factor] = cur;

    return acc + number * factor;
  }, 0);

//0 + (1 * 1) = 1
//1 + (3 * 5) = 16
//16 + (2 * 7) = 30

const multiple = sumProduct([[1, 1], [3, 5], [2, 7]]);

console.log(multiple); // 30
