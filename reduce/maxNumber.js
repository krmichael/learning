const filterByMax = array =>
  array.reduce((acc, cur) => {
    if (!acc || acc < cur) return cur;
    return acc;
  }, null);

console.log({ max: filterByMax([3, 1, 5, 12]) });
